package com.epam.task2;

import com.epam.task2.config.ConfigurationA;
import com.epam.task2.order.Flower;
import com.epam.task2.order.FlowerWrapper;
import com.epam.task2.other.OtherBeanA;
import com.epam.task2.other.OtherBeanB;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ConfigurationA.class);
        var flowerWrapper = context.getBean(FlowerWrapper.class);
        for (Flower flower : flowerWrapper.getFlowers()) {
            System.out.println("Flower - " + flower.getName());
        }

        System.out.println(context.getBean(OtherBeanA.class));
        System.out.println(context.getBean(OtherBeanA.class));
        System.out.println(context.getBean(OtherBeanB.class));
        System.out.println(context.getBean(OtherBeanB.class));
        System.out.println(context.getBean("otherBeanC"));
        System.out.println(context.getBean("otherBeanC"));
    }
}
