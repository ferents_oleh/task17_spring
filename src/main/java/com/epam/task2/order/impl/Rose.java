package com.epam.task2.order.impl;

import com.epam.task2.order.Flower;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Order(1)
@Primary
public class Rose implements Flower {
    private final String name = "Rose";

    @Override
    public String getName() {
        return name;
    }
}
