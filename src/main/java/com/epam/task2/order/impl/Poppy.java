package com.epam.task2.order.impl;

import com.epam.task2.order.Flower;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Order(2)
@Qualifier("poppy")
public class Poppy implements Flower {
    private final String name = "Poppy";

    @Override
    public String getName() {
        return name;
    }


}
