package com.epam.task2.order.impl;

import com.epam.task2.order.Flower;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Order(3)
@Qualifier("bluebell")
public class Bluebell implements Flower {
    private final String name = "Bluebell";

    @Override
    public String getName() {
        return name;
    }
}
