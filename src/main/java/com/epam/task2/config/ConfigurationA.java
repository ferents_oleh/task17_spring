package com.epam.task2.config;

import com.epam.task2.beans1.BeanA;
import com.epam.task2.beans1.BeanB;
import com.epam.task2.beans1.BeanC;
import com.epam.task2.beans2.CatAnimal;
import com.epam.task2.beans3.BeanE;
import com.epam.task2.other.OtherBeanA;
import com.epam.task2.other.OtherBeanB;
import com.epam.task2.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import(FlowerConfiguration.class)
@ComponentScan("com.epam.task2.beans1")
public class ConfigurationA {
    @Bean
    public BeanA beanA() {
        return new BeanA();
    }

    @Bean
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean
    public BeanC beanC() {
        return new BeanC(otherBeanC());
    }

    @Bean
    public CatAnimal catAnimal() {
        return new CatAnimal();
    }

    @Bean
    public BeanE beanE() {
        return new BeanE();
    }

    @Bean
    @Scope("singleton")
    public OtherBeanA otherBeanA() {
        return new OtherBeanA();
    }

    @Bean
    @Scope("prototype")
    public OtherBeanB otherBeanB() {
        return new OtherBeanB();
    }

    @Bean
    @Scope("singleton")
    public OtherBeanC otherBeanC() {
        return new OtherBeanC();
    }
}
