package com.epam.task2.config;

import com.epam.task2.order.FlowerWrapper;
import com.epam.task2.order.impl.Bluebell;
import com.epam.task2.order.impl.Poppy;
import com.epam.task2.order.impl.Rose;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class FlowerConfiguration {

    @Bean
    public Bluebell bluebell() {
        return new Bluebell();
    }

    @Bean
    public Poppy poppy() {
        return new Poppy();
    }

    @Bean
    public Rose rose() {
        return new Rose();
    }

    @Bean
    public FlowerWrapper flowerWrapper() {
        return new FlowerWrapper();
    }
}
