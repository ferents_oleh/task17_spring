package com.epam.task2.other;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanA {
    private String name = "Other bean A";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
