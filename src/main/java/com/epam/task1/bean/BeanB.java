package com.epam.task1.bean;

import com.epam.task1.validation.BeanValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BeanB implements BeanValidator {
    @Value("${beanB.name}")
    private String name;

    @Value("${beanB.value}")
    private int value;

    public void init() {
        System.out.println("Bean B init method");
    }

    public void customInit() {
        System.out.println("customInit()");
    }

    private void destroy() {
        System.out.println("Bean B destroy method");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean validate() {
        return name.isEmpty() || value >= 0;
    }
}
