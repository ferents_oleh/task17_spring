package com.epam.task1.config;

import com.epam.task1.bean.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

@Configuration
@Import(ConfigurationB.class)
@PropertySource("classpath:application.properties")
public class ConfigurationA {
    @Bean(initMethod = "init", destroyMethod = "destroy")
    @DependsOn("beanD")
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean(name = "beanA1")
    public BeanA beanA(BeanB beanB, BeanC beanC) {
        return new BeanA(beanB, beanC);
    }

    @Bean(name = "beanA2")
    public BeanA beanA(BeanB beanB, BeanD beanD) {
        return new BeanA(beanB, beanD);
    }

    @Bean(name = "beanA3")
    public BeanA beanA(BeanC beanC, BeanD beanD) {
        return new BeanA(beanC, beanD);
    }

    @Bean
    public BeanE beanE(@Qualifier("beanA1") BeanA beanA) {
        return new BeanE(beanA);
    }

    @Bean
    public CustomBeanFactoryPostProcessor factoryPostProcessor() {
        return new CustomBeanFactoryPostProcessor();
    }
}
