package com.epam.task1.validation;

public interface BeanValidator {
    boolean validate();
}
